# snake-rs-bevy
Simple Snake clone
## Description
This projects purpose is to learn Rust and evaluate Bevy as a game engine.
I am following this great tutorial from Marcus Buffet (https://mbuffett.com/posts/bevy-snake-tutorial/) and will be adding extra functionality (e.g. sprite rendering, score, animations) along the way.
